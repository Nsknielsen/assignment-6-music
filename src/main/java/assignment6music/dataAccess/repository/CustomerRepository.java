package assignment6music.dataAccess.repository;


import assignment6music.dataAccess.dbConnection.ConnectionUrlHelper;
import assignment6music.dataAccess.model.Customer;
import assignment6music.dataAccess.model.CustomerCountry;
import assignment6music.dataAccess.model.CustomerGenre;
import assignment6music.dataAccess.model.CustomerSpender;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;

@Service
public class CustomerRepository implements ICustomerRepository {

    protected Connection connection = null;

    // returns a list of all customers
    public ArrayList<Customer> getAllCustomers() {

        //array to return
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer");
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object, populate customers array
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customers.add(customer);
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customers;
    }

    //gets one customer by id
    public Customer getCustomerById(int customerId) {

        Customer customer = null;

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer WHERE CustomerId = ?"
            );
            preparedStatement.setInt(1, customerId);
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customer;
    }

    //gets one customer by name
    public Customer getCustomerByName(String firstName, String lastName) {

        Customer customer = null;

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email"
                            + " FROM Customer"
                            + " WHERE FirstName LIKE ? AND LastName LIKE ?"
            );
            preparedStatement.setString(1, "%" + firstName + "%");
            preparedStatement.setString(2, "%" + lastName + "%");
            System.out.println("statement prepared: " + preparedStatement);

            //execute query
            // Will use the first result in case of more than one matching record.
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object
            customer = new Customer(
                    resultSet.getInt("CustomerId"),
                    resultSet.getString("FirstName"),
                    resultSet.getString("LastName"),
                    resultSet.getString("Country"),
                    resultSet.getString("PostalCode"),
                    resultSet.getString("Phone"),
                    resultSet.getString("Email")
            );

            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customer;
    }

    // returns a list of all customers with offset and limit
    public ArrayList<Customer> getSubsetOfCustomers(Integer offset, Integer limit) {

        //array to return
        ArrayList<Customer> customers = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement(
                    "SELECT CustomerId, FirstName, LastName, Country, PostalCode, Phone, Email FROM Customer LIMIT ?,?");
            preparedStatement.setInt(1, offset);
            preparedStatement.setInt(2, limit);

            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object, populate customers array
            while (resultSet.next()) {
                Customer customer = new Customer(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getString("Country"),
                        resultSet.getString("PostalCode"),
                        resultSet.getString("Phone"),
                        resultSet.getString("Email")
                );
                customers.add(customer);
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customers;
    }

    public Boolean addCustomer(Customer customer) {
        Boolean success = false;
        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            String SQL = "INSERT INTO Customer (FirstName, LastName, Country, PostalCode, Phone, Email)"
                    + " VALUES (?,?,?,?,?,?)";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, customer.firstName);
            preparedStatement.setString(2, customer.lastName);
            preparedStatement.setString(3, customer.country);
            preparedStatement.setString(4, customer.postalCode);
            preparedStatement.setString(5, customer.phone);
            preparedStatement.setString(6, customer.email);

            //execute query and update boolean to reflect success/failure
            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                connection.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

    // gets number of customers per country
    public ArrayList<CustomerCountry> getCustomersPerCountry() {
        //array to return
        ArrayList<CustomerCountry> customers = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            String SQL = "SELECT COUNT(*) numCustomers, Country FROM Customer"
                    + " GROUP BY Country ORDER BY numCustomers DESC";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object, populate customers array
            while (resultSet.next()) {
                CustomerCountry customerCountry = new CustomerCountry(
                        resultSet.getInt("numCustomers"),
                        resultSet.getString("Country")
                );
                customers.add(customerCountry);
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customers;
    }

    public ArrayList<CustomerSpender> getCustomerSpender() {
        //array to return
        ArrayList<CustomerSpender> customerSpenders = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            String SQL = "SELECT CustomerId, FirstName, LastName, ROUND(SUM(Total),2) invoiceTotal " +
                    "        FROM Invoice JOIN Customer USING(CustomerId) " +
                    "        GROUP BY CustomerId ORDER BY invoiceTotal DESC LIMIT 20 ";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                CustomerSpender customerSpender = new CustomerSpender(
                        resultSet.getInt("CustomerId"),
                        resultSet.getString("FirstName"),
                        resultSet.getString("LastName"),
                        resultSet.getDouble("invoiceTotal")
                );
                customerSpenders.add(customerSpender);
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return customerSpenders;
    }

    public ArrayList<CustomerGenre> getCustomerFavouriteGenre(int customerId) {

        ArrayList<CustomerGenre> topGenres = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //sql query
            String CrazyLongSql =
                    "SELECT g.name GenreName, COUNT(*) GenreCount"
                            + " FROM Invoice"
                            + " JOIN InvoiceLine using(InvoiceId)"
                            + " JOIN Track USING(TrackId)"
                            + " JOIN Genre g USING(GenreId)"
                            + " WHERE CustomerId = ?"
                            + " GROUP by GenreId"
                            + " HAVING GenreCount ="
                            + " (SELECT count(*) cnt"
                            + " FROM Invoice"
                            + " JOIN InvoiceLine USING(InvoiceId)"
                            + " JOIN Track USING(TrackId)"
                            + " WHERE CustomerId = ?"
                            + " GROUP BY GenreId"
                            + " ORDER BY cnt desc"
                            + " LIMIT 1)";

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement(CrazyLongSql);
            preparedStatement.setInt(1, customerId);
            preparedStatement.setInt(2, customerId);
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            //convert result to strings for Customer object
            while (resultSet.next()) {
                CustomerGenre customerGenre = new CustomerGenre(
                        resultSet.getString("GenreName"),
                        resultSet.getInt("GenreCount")
                );
                topGenres.add(customerGenre);
            }

            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return topGenres;
    }

    public Boolean updateCustomer(int customerId, Customer customer) {
        Boolean success = false;
        if (customer.customerId != customerId) {
            return success;
        }
        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            String SQL = "UPDATE Customer"
                    + " SET FirstName=?, LastName=?, Country=?, PostalCode=?, Phone=?, Email=?"
                    + " WHERE CustomerId=?";

            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, customer.firstName);
            preparedStatement.setString(2, customer.lastName);
            preparedStatement.setString(3, customer.country);
            preparedStatement.setString(4, customer.postalCode);
            preparedStatement.setString(5, customer.phone);
            preparedStatement.setString(6, customer.email);
            preparedStatement.setInt(7, customerId);

            //execute query and update boolean to reflect success/failure
            int result = preparedStatement.executeUpdate();
            success = (result != 0);

        } catch (Exception exception) {
            System.out.println(exception.toString());
        } finally {
            try {
                connection.close();
            } catch (Exception exception) {
                System.out.println(exception.toString());
            }
        }
        return success;
    }

}

