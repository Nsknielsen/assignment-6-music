package assignment6music.dataAccess.repository;


import assignment6music.dataAccess.model.TrackInfo;
import org.springframework.stereotype.Repository;

import java.util.ArrayList;

@Repository
public interface IMusicRepository {

    public ArrayList<String> getRandomArtists();

    public ArrayList<String> getRandomSongs();

    public ArrayList<String> getRandomGenres();

    public ArrayList<TrackInfo> getInfoByTrackName(
            String trackSearch
    );
}
