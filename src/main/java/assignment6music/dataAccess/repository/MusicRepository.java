package assignment6music.dataAccess.repository;

import assignment6music.dataAccess.dbConnection.ConnectionUrlHelper;
import assignment6music.dataAccess.model.TrackInfo;
import org.springframework.stereotype.Service;

import java.sql.*;
import java.util.ArrayList;

@Service
public class MusicRepository implements IMusicRepository{

    protected Connection connection = null;

    public ArrayList<String> getRandomArtists() {

        //array to return
        ArrayList<String> artists = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Artist ORDER BY random() LIMIT 5");
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                artists.add(resultSet.getString("Name"));
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }


        return artists;
    }

    public ArrayList<String> getRandomSongs() {

        //array to return
        ArrayList<String> songs = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Track ORDER BY random() LIMIT 5");
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                songs.add(resultSet.getString("Name"));
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return songs;
    }

    public ArrayList<String> getRandomGenres() {

        //array to return
        ArrayList<String> genres = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            PreparedStatement preparedStatement = connection.prepareStatement("SELECT Name FROM Genre ORDER BY random() LIMIT 5");
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                genres.add(resultSet.getString("Name"));
            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }

        return genres;
    }

    public ArrayList<TrackInfo> getInfoByTrackName(String trackSearch) {

        //array to return
        ArrayList<TrackInfo> tracks = new ArrayList<>();

        try {
            //connect to db
            connection = DriverManager.getConnection(ConnectionUrlHelper.dbUrl);
            System.out.println("conn established");

            //prepare sql query
            String SQL = "SELECT t.Name Track, ar.Name Artist, al.Title Album, g.Name Genre" +
                    " FROM" +
                    "  Track t" +
                    "   JOIN Album al USING(AlbumId)" +
                    "   JOIN Artist ar USING(ArtistId)" +
                    "   JOIN Genre g USING(GenreId)" +
                    " WHERE" +
                    "  t.Name LIKE ?";
            PreparedStatement preparedStatement = connection.prepareStatement(SQL);
            preparedStatement.setString(1, '%' + trackSearch + '%');
            System.out.println("statement prepared");

            //execute query
            ResultSet resultSet = preparedStatement.executeQuery();

            while (resultSet.next()) {
                TrackInfo trackInfo = new TrackInfo(
                        resultSet.getString("Track"),
                        resultSet.getString("Artist"),
                        resultSet.getString("Album"),
                        resultSet.getString("Genre")
                );
                tracks.add(trackInfo);

            }
            //exception handling and closing connection
        } catch (SQLException sqlException) {
            System.out.println(sqlException.toString());
        } finally {
            try {
                connection.close();
            } catch (SQLException sqlException) {
                System.out.println(sqlException.toString());
            }
        }
        return tracks;
    }
}
