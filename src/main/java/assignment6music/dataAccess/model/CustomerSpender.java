package assignment6music.dataAccess.model;

public class CustomerSpender {
    public int customerId;
    public String firstName;
    public String lastName;
    public Double total;

    public CustomerSpender(int customerId, String firstName, String lastName, Double total) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.total = total;
    }
}
