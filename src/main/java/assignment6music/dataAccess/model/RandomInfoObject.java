package assignment6music.dataAccess.model;

import java.util.ArrayList;

public class RandomInfoObject {
    public ArrayList<String> artists;
    public ArrayList<String> songs;
    public ArrayList<String> genres;


    public RandomInfoObject(ArrayList<String> artists, ArrayList<String> songs, ArrayList<String> genres) {
        this.artists = artists;
        this.songs = songs;
        this.genres = genres;
    }
}
