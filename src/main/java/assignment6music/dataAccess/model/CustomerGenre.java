package assignment6music.dataAccess.model;

public class CustomerGenre {
    public String genreName;
    public int genreCount;

    public CustomerGenre(String genreName, int genreCount) {
        this.genreName = genreName;
        this.genreCount = genreCount;
    }
}
