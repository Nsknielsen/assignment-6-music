package assignment6music.dataAccess.model;

public class TrackInfo {
    public String track;
    public String artist;
    public String album;
    public String genre;

    public TrackInfo(String track, String artist, String album, String genre) {
        this.track = track;
        this.artist = artist;
        this.album = album;
        this.genre = genre;
    }
}
