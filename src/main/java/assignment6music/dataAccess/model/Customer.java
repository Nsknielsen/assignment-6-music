package assignment6music.dataAccess.model;

//POJO model to hold customer data
public class Customer {
    public int customerId; //might be redundant - depends on autoincrement in db?
    public String firstName;
    public String lastName;
    public String country;
    public String postalCode;
    public String phone;
    public String email;

    public Customer(int customerId, String firstName, String lastName,String country, String postalCode, String phone, String email){
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.country = country;
        this.postalCode = postalCode;
        this.phone = phone;
        this.email = email;
    }
}
