package assignment6music;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

//Entry point for spring application.
//NOTE this class needs to be in the root folder of the java part of the project, otherwise Spring can't find
//the endpoints in controller classes.
@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }
}
